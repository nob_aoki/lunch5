/** 
 * @file lunch5_splashscreen.h
 * @brief ここに説明を書く
 * 
 * @note 2011.09.14 (株)トーメーコーポレーション  青木 伸頼 作成
 */
#ifndef __ROCTIA_SPLASHSCREEN_H__
#define __ROCTIA_SPLASHSCREEN_H__

#include <windows.h>
#include <commctrl.h>

#include "lunch5_wndbase.h"

//class CImageLib;
#include <imagelib.h>

/**
 * スプラッシュスクリーン用ウインドウ管理クラス
 */
class CSplashScreenWnd : public CWindowBase
{
public:
	/// コンストラクタ
	CSplashScreenWnd()
		:CWindowBase(NULL) {}

	/// コンストラクタ
	CSplashScreenWnd(HWND hWnd)
		:CWindowBase(hWnd) {}

	/// デストラクタ
	~CSplashScreenWnd() {}

	/// ウインドウが生成されているかどうか
	//int IsOK() {return (hWnd_!=0);}

	/// ウインドウのハンドルを返す
	//operator HWND() {return hWnd_;}

	/**
	 * ウインドウを表示するかどうか
	 *
	 * @param b 0:隠す, 1:表示する
	 */
	void Show(int b) {
		ShowWindow((b!=0)?SW_SHOW/*NORMAL*/:SW_HIDE);
	//	BringWindowToTop(GetParent(hWnd_));
		::SetWindowPos(GetParent(), HWND_TOP,
			0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);	// 親が背面に行ってしまうのを抑止。
	}
	void ForceHide(){
		ShowWindow(SW_HIDE);
	}

	void Close(){SendMessage(WM_CLOSE,0,0);}

	void OnCreate();
	void OnPaint(HDC hdc);

	static LRESULT CALLBACK WndProc(HWND hWnd,UINT msg,WPARAM wp,LPARAM lp);
	static CSplashScreenWnd*FromHWND(HWND hWnd) {
		return (CSplashScreenWnd*)::GetWindowLongPtr(hWnd,GWLP_USERDATA);
	}
	static HWND Create(HWND hParent, HINSTANCE hInst);

private:
	// HWND hWnd_;			///< ウインドウのハンドル
	CImageLib image_;

	static const char*szClassName;
};

// extern CSplashScreenWnd gSplashWnd;
extern CSplashScreenWnd*gSplashWnd;

#endif  // __ROCTIA_SPLASHSCREEN_H__
