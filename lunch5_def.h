#ifndef __LUNCH5_DEF_H__
# define __LUNCH5_DEF_H__

enum {
    ANIM_FIX = 0,
    ANIM_LEFT_FIX,
    ANIM_RIGHT_FIX,
    ANIM_SLIDE,
    ANIM_SHORTEN,
};

enum {
    VISIBLE = 0,
    DISABLE,
    HIDE,
}

enum {
    CAPTION_TOP,
    CAPTION_BOTTOM,
};

enum {
    WINDOW_POS_RIGHT = -1,  // it is doubtful that -1 is good or not.
    WINDOW_POS_BOTTOM = -1,  // it is doubtful that -1 is good or not.
};

enum {
    MOUSE_NONE = 0,
    MOUSE_IN,
    MOUSE_DOWN,
    MOUSE_DRAG,
    MOUSE_DBLCLK,
};


#endif  // __LUNCH5_DEF_H__
