#include "lunch5_config.h"

#include <windows.h>

#include <string.h>

#include <fio.h>
#include <sax.h>

#include "lunch5_anim.h"
#include "lunch5_item.h"
#include "lunch5_sax.h"
#include "lunch5_skin.h"


// -------------------------------------
// CLunch5Config
// -------------------------------------

CLunch5Config::CLunch5Config()
    :right_button_(BTN_ENABLED), left_button_(BTN_ENABLED),
    caption_align_(ALIGN_TOP),
    caption_fg_color_(0xffffffu), caption_bg_color_(0xff2010u),
    checkdisk_(0), priority_(NORMAL_PRIORITY_CLASS),
    hover_caption_(1), hover_activate_(0), hover_act_time_(0),
    must_be_in_desktop_(0), closed_window_(0), warn_slight_move_(0),
    fix_position_(0)
{
}

CLunch5Config::~CLunch5Config()
{
}

/** 
 * ここに説明を書く
 * 
 * @param file 引数'*file'の説明
 *
 * @return ここに説明を書く
 *
 * @author (株)トーメーコーポレーション  青木 伸頼
 * @date 2012.12.03
 * @note 2012.12.03 (株)トーメーコーポレーション  青木 伸頼 作成
 * @note status:未着工、途中、テスト待ち、完成1、リファクタ、完成2
 */
int CLunch5Config::Open(CStdio *file)
{
    if (file == NULL || file->IsOK() == 0) {
        return 2;
    }

    CXMLParser parser;
    CLunch5Sax sax(this);
    parser.setSax(&sax);
    int ret = parser._parse(*file);
    if (ret != 0) {
/* @retval 0 正常終了。
 * @retval 1 SAXが登録されていない。
 * @retval 2 ファイルが存在しない。
 * @retval 3 ファイルの終端。
 * @retval 4 特殊文字の解析エラー
 * @retval 5 アトリビュートの解析エラー
 * @retval 6 SAXのエラー
 * @retval 7 構文のエラー
 */
    } else {
        return 0;
    }
}

/** 
 * ここに説明を書く
 * 
 * @param file 引数'*file'の説明
 * @return ここに説明を書く
 * @author (株)トーメーコーポレーション  青木 伸頼
 * @date 2012.12.03
 * @note 2012.12.03 (株)トーメーコーポレーション  青木 伸頼 作成
 * @note status:未着工、途中、テスト待ち、完成1、リファクタ、完成2
 */
int CLunch5Config::Save(CStdio *file)
{
    file->Puts("<?xml version=\"1.0\" encoding=\"Shift_JIS\" ?>\n");

    file->Puts("<Lunch5 version=\"2.1\">\n");

    anim_.WriteXML(file);

    file->Puts(" <configuration>\n");
    file->Print("  <checkdisk value=\"%d\"/>", checkdisk_);
    file->Print("  <traytext>%s</traytext>\n", traytext_);
    file->Print("  <caption align=\"%s\" color=\"#%06X\" bgcolor=\"#%06X\"/>",
                (caption_align_ == ALIGN_TOP) ? "top" : "bottom",
                SwapRB(caption_fg_color_), SwapRB(caption_bg_color_));
    file->Puts(" </configuration>\n");

    file->Print(" <wnd depress=\"%d\" fix=\"%d\" warn=\"%s\">\n",
                must_be_in_desktop_, fix_position_, warn_slight_move_);
    file->Puts("  <left>");
    switch (left_button_) {
    case BTN_DISABLED:
        file->Puts("disable");
        break;
    case BTN_HIDDEN:
        file->Puts("hide");
        break;
    case BTN_ENABLED:
    default:
        ;
    }
    file->Puts("</left>\n");
    file->Puts("  <right>");
    switch (right_button_) {
    case BTN_DISABLED:
        file->Puts("disable");
        break;
    case BTN_HIDDEN:
        file->Puts("hide");
        break;
    case BTN_ENABLED:
    default:
        ;
    }
    file->Puts("</right>\n");
    file->Print("  <x>%s</x>\n", pos_x_);
    file->Print("  <y>%s</y>\n", pos_y_);

    skin_.WriteXML(file);

    file->Print("  <Hover tooltip=\"%d\" active=\"%d\" time=\"%d\"/>\n",
                hover_caption_, hover_activate_, hover_act_time_);
    file->Puts(" </wnd>");

    itemarray_.WriteXML(file);

    file->Puts("</Lunch5>\n");
}
