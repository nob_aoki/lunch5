/** 
 * @file lunch5_wndbase.h
 * @brief ここに説明を書く
 */
#ifndef __lunch5_WNDBASE_H__
# define __lunch5_WNDBASE_H__

#include <Windows.h>

class CWindowBase
{
public:
    CWindowBase()
        :hWnd_(NULL) {
    }
    explicit CWindowBase(HWND hWnd)
        :hWnd_(hWnd) {
        //    hWnd_ = hWnd;
    }
    virtual ~CWindowBase() {}
#if 0
    void OnClose();
    void OnCreate(LPCREATESTRUCT lpcs);
    void OnDrawItem(int nID, DRAWITEMSTRUCT*pdi);
    void OnDropFiles(HDROP hDrop);
    void OnGetMinMaxInfo(LPMINMAXINFO pmmi);
    void OnInitDialog(LPARAM lp);
    void OnKeyDown(UINT nKey, LPARAM code);
    void OnMouseMove(UINT flag, int x, int y);
    int OnNotify(LPNMHDR pnmhdr);
    void OnPaint(HDC hDC);
    void OnLButtonDblClk(UINT flag, int x, int y);
    void OnLButtonDown(UINT flag, int x, int y);
    void OnLButtonUp(UINT flag, int x, int y);
    void OnRButtonDown(UINT flag, int x, int y);
    void OnRButtonUp(UINT flag, int x, int y);
    void OnSize(UINT uFlag, int width, int height);
    void OnTimer(int ID);
    int OnCommand(WPARAM, LPARAM);
    int OnCommand(int code, int id, HWND hFrom);
#endif
    void Register() {::SetWindowLongPtr(hWnd_, GWLP_USERDATA, (INT_PTR)this);}
    void Register(HWND hWnd) {
        hWnd_ = hWnd;
        ::SetWindowLongPtr(hWnd_, GWLP_USERDATA, (INT_PTR)this);
    }

    /// ウインドウが生成されているかどうか
    int IsOK() {return (hWnd_ != 0);}

    /// ウインドウのハンドルを返す
    HWND GetHWND() {return hWnd_;}

    /// ウインドウのハンドルを返す
    operator HWND() {return hWnd_;}

    /// 親ウインドウのハンドルを返す
    HWND GetParent() {return ::GetParent(hWnd_);}

    /// 子ウインドウのハンドルを返す
    HWND GetDlgItem(int id) {return ::GetDlgItem(hWnd_, id);}
    UINT GetDlgItemInt(int id, BOOL*ptrans, BOOL Signed) {
        return ::GetDlgItemInt(hWnd_, id, ptrans, Signed);
    }
    UINT GetDlgItemText(int id, char* str, int size) {
        return ::GetDlgItemText(hWnd_, id, str, size);
    }
    BOOL SetDlgItemInt(int id, UINT val, BOOL Signed) {
        return ::SetDlgItemInt(hWnd_, id, val, Signed);
    }
    BOOL SetDlgItemText(int id, const char*str) {
        return ::SetDlgItemText(hWnd_, id, str);
    }

    void ShowWindow(int flgShow) {::ShowWindow(hWnd_, flgShow);}
    void EnableWindow(int bEnable) {::EnableWindow(hWnd_, bEnable);}

    void SetWindowPos(
                HWND hWndInsertAfter, int x, int y, int cx, int cy, int flg) {
        ::SetWindowPos(hWnd_, hWndInsertAfter, x, y, cx, cy, flg);
    }
    void GetWindowRect(RECT*prw) {::GetWindowRect(hWnd_, prw);}
    void GetClientRect(RECT*prc) {::GetClientRect(hWnd_, prc);}

    void ScreenToClient(POINT*ppnt) {
        ::ScreenToClient(hWnd_, ppnt);
    }
    void ClientToScreen(POINT*ppnt) {
        ::ClientToScreen(hWnd_, ppnt);
    }

    void DestroyWindow() {::DestroyWindow(hWnd_);}

    LRESULT SendMessage(UINT msg, WPARAM wp, LPARAM lp) {
        return ::SendMessage(hWnd_, msg, wp, lp);
    }
    LRESULT PostMessage(UINT msg, WPARAM wp, LPARAM lp) {
        return ::PostMessage(hWnd_, msg, wp, lp);
    }

    void SetTimer(UINT_PTR id, UINT uElapse, TIMERPROC tproc) {
        ::SetTimer(hWnd_, id, uElapse, tproc);
    }
    void KillTimer(int id) {::KillTimer(hWnd_, id);}

    int MessageBox(const char *msg, const char *title, int flg) {
        return ::MessageBox(hWnd_, msg, title, flg);
    }

    HDC GetDC() {return ::GetDC(hWnd_);}
    void ReleaseDC(HDC hdc) {::ReleaseDC(hWnd_, hdc);}

    int GetScrollInfo(int fnBar, SCROLLINFO *psi) {
        return ::GetScrollInfo(hWnd_, fnBar, psi);
    }
    int SetScrollInfo(int fnBar, SCROLLINFO *psi, int bRedraw) {
        return ::SetScrollInfo(hWnd_, fnBar, psi, bRedraw);
    }

    BOOL RedrawWindow(const RECT *lprcUpdate, HRGN hrgnUpdate, UINT flags) {
        return ::RedrawWindow(hWnd_, lprcUpdate, hrgnUpdate, flags);
    }
    void InvalidateRect(const RECT *prc, int bRedraw) {
        ::InvalidateRect(hWnd_, prc, bRedraw);
    }
    void Invalidate(int bRedraw = FALSE) {
        ::InvalidateRect(hWnd_, NULL, bRedraw);
    }

    HWND SetCapture() {return ::SetCapture(hWnd_);}
    HWND GetCapture() {return ::GetCapture();}
    int ReleaseCapture() {return ::ReleaseCapture();}

    void SetFocus() {::SetFocus(hWnd_);}

    LONG_PTR GetWindowLongPtr(int index) {
        return ::GetWindowLongPtr(hWnd_, index);
    }
    LONG_PTR SetWindowLongPtr(int index, INT_PTR param) {
        return ::SetWindowLongPtr(hWnd_, index, param);
    }

    void SetWindowText(const char *title) {::SetWindowText(hWnd_, title);}
    void GetWindowText(char *buf, int size) {::GetWindowText(hWnd_, buf, size);}
public:
    static CWindowBase*FromHWND(HWND hWnd) {
        return reinterpret_cast<CWindowBase*>(
            ::GetWindowLongPtr(hWnd, GWLP_USERDATA));
    }
    static HWND Create(HINSTANCE hInstance);
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp);

protected:
    HWND hWnd_;

private:
    // static const char* szClassName;
    // static const char* szTitle;
};


#endif  // __lunch5_WNDBASE_H__
