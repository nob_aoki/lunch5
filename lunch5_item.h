#ifndef __LUNCH5_ITEM_H__
# define __LUNCH5_ITEM_H__

#include <vector>

#include <cmnstring.h>


class CItem
{
public:
    CItem()
        :nIcon_(0) {}
    CItem(const CItem &rhs) {
        path_cmd_ = rhs.path_cmd_;
        cmdline_ = rhs.cmdline_;
        path_icon_ = rhs.path_icon_;
        nIcon_ = rhs.nIcon_;
        text_face_ = rhs.text_face_;
        path_dir_ = rhs.path_dir_;
    }
    ~CItem() {}

    const char* GetCmdPath() {return path_cmd_;}
    const char* GetCmdline() {return cmdline_;}
    const char* GetIconPath() {return path_icon_;}
    int         GetIconIndex() {return nIcon_;}
    const char* GetFaceText() {return text_face_;}
    const char* GetDirectory() {return path_dir_;}

    void SetCmdPath(const char* path) {path_cmd_ = path;}
    void SetCmdline(const char* cmdline) {cmdline_ = cmdline;}
    void SetIconPath(const char* path) {path_icon_ = path;}
    int  SetIconIndex(int idx) {nIcon_ = idx;}
    void SetFaceText(const char* text) {text_face_ = text;}
    void SetDirectory(const char* dir) {path_dir_ = dir;}

    HICON GetIcon() {
        return ExtractIcon(GetModuleHandle(NULL), path_icon_, nIcon_);
        // ExtractIconEx()を使ったほうがいいかもしれません。
    }
    int Execute(HWND hWnd = NULL) {
        return (ShellExecute(hWnd, "open", path_cmd_,
                        cmdline_, path_dir_, SW_SHOWNORMAL) == (HINSTANCE)32);
        // CreateProcessを使ったほうがいいかもしれません。
    }

    int WriteXML(CStdio *file) {
        file->Puts(" <button>\n");
        file->Print("  <face>%s<face>", text_face_);
        file->Print("  <path>%s</path>\n", path_cmd_);
        file->Print("  <icon num=\"%d\">%s</icon>\n", nIcon_, path_icon_);
        file->Print("  <cmdline>%s</cmdline>\n", cmdline_);
        file->Print("  <dir>%s</dir>\n", path_dir_);
        file->Puts(" </button>\n");
    }

private:
    CString text_face_;  // 表示名

    CString path_cmd_;   // 実行するファイルのパス
    CString cmdline_;    // コマンドラインオプション
    CString path_dir_;   // 実行ディレクトリ

    CString path_icon_;  // アイコンのパス
    int     nIcon_;      // アイコンのインデックス
};


class CItemArray
{
public:
    CItemArray() {}
    ~CItemArray() {
        if (items_.size() != 0) {
            std::vector<CItem*>::iterator it;
            for (it = items_.begin(); it < items_.end(); ++it) {
                delete *it;
            }
        }
        items_.clear();
    }

    void Init() {
        if (items_.size() != 0) {
            std::vector<CItem*>::iterator it;
            for (it = items_.begin(); it < items_.end(); ++it) {
                delete *it;
            }
        }
        items_.clear();
    }

    int Size() {return static_cast<int>(items_.size());}
    void Add(CItem *pitem) {
        items_.push_back(pitem);
    }
    CItem* GetAt(int idx) {
        if (idx>= Size()) {
            return NULL;
        }
        return items_[idx];
    }
    void WriteXML(CStdio *file) {
        if (items_.size() != 0) {
            std::vector<CItem*>::iterator it;
            for (it = items_.begin(); it < items_.end(); ++it) {
                (*it)->WriteXML(file);
            }
        }
    }

private:
    std::vector<CItem*> items_;
};


#endif  // __LUNCH5_ITEM_H__
