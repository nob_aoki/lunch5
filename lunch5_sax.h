#ifndef __LUNCH5_SAX_H__
#define __LUNCH5_SAX_H__

#include <sax.h>

#include "lunch5_config.h"
#include "lunch5_item.h"


class CLunch5Sax : public CSax
{
public:
    enum {
        STAGE_NONE = 0,

        STAGE_LUNCH,

        STAGE_ANIM,
        STAGE_ANIM_MOVE,
        STAGE_ANIM_DRAW,

        STAGE_BUTTON,
        STAGE_BUTTON_CMDLINE,
        STAGE_BUTTON_DIR,
        STAGE_BUTTON_FACE,
        STAGE_BUTTON_ICON,
        STAGE_BUTTON_PATH,

        STAGE_CONFIG,
        STAGE_CONFIG_CAPTION,
        STAGE_CONFIG_CHECKDISK,
        STAGE_CONFIG_PASSWD,
        STAGE_CONFIG_PRIORITY,
        STAGE_CONFIG_TRAYTEXT,

        STAGE_WND,
        STAGE_WND_HOVER,
        STAGE_WND_LEFT,
        STAGE_WND_RIGHT,
        STAGE_WND_SKIN,
        STAGE_WND_X,
        STAGE_WND_Y,

        STAGE_MAX,
    };
public:
    explicit CLunch5Sax(CLunch5Config *pconfig)
        :pconfig_(pconfig), pitem_(NULL), nstage_(STAGE_NONE) {}
    virtual ~CLunch5Sax() {
        if (pitem_ != NULL) {
            delete pitem_;
            pitem_ = NULL;
        }
    }

    virtual void xmlDeclare(CAttribArray &atrary) {
        // puts("xml-document");
        nstage_ = STAGE_NONE;
        if (pitem_ != NULL) {
            delete pitem_;
            pitem_ = NULL;
        }
    }

    virtual void documentStart() {
        // puts("--doc_start");
    }
    virtual void documentEnd() {
        // puts("--doc_end");
    }

    virtual int elementStart(const char *name, CAttribArray &atrary);
    virtual int elementEnd(const char *name);

    virtual int characters(const char *str);

public:
    void SetConfig(CLunch5Config *pconfig) {
        pconfig_ = pconfig;
    }

private:
    CLunch5Config *pconfig_;
    int nstage_;
    CItem *pitem_;

public:
    static const char*tags_[];
};

#endif  // __LUNCH5_SAX_H__
