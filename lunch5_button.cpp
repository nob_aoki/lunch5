/** 
 * @file lunch5_button.cpp
 * @brief ここに説明を書く
 * 
 */

#include <windows.h>

#include "lunch5_button.h"

#include <cstdio>
#include <cmath>

#include <commctrl.h>

#include <wndutil.h>
#include <imagelib.h>
#include <fio.h>

#include "resource.h"

#include "lunch5_main.h"

#include "lunch5_wnd.h"


#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif

//------------------------------------------------
// 定数
//------------------------------------------------

const char* CMyButton::szClassName = "lunch5_button";
const char* CMyButton::szTitle = "";

//------------------------------------------------
// 変数
//------------------------------------------------



#if 0
void CMyButton::OnTimer(int ID)
{

}

/**
 * ウインドウの初期化
 */
void CMyButton::OnCreate(LPCREATESTRUCT lpcs)
{
	//EnableWindow(FALSE);
}

void CMyButton::OnSize(UINT/*uFlag*/, int/* width*/, int/* height*/)
{
	int width_left, tbheight;
	RECT rc1, rc2, rc3;  // ,rc4,rc5;
}


void CMyButton::OnDropFiles(HDROP hDrop)
{
/*	char filename[MAX_PATH];

	DragQueryFile(hDrop, 0, filename, sizeof(filename));
	DragFinish(hDrop);

	OpenFile(filename);*/
}


int CMyButton::OnNotify(LPNMHDR pnmhdr)
{
	return 0;
}

void CMyButton::OnPaint()
{
	PAINTSTRUCT ps;
	CDC dc(BeginPaint(hWnd_, &ps));

    RECT rc;
    GetWIndowClient(&rc);
    image_.DrawEx(dc, 0, 0, rc.right, rc.bottom,
                  ox, oy, BUTTON_WIDTH, BUTTON_HEIGHT);

	EndPaint(hWnd_, &ps);
}

void CMyButton::OnClose()
{
	DestroyWindow();
}

/**
*	WndProc
*/
LRESULT CALLBACK CMyButton::WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
	CMyButton*pwnd;
	switch (msg) {
	//case WM_SIZE:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	pwnd->OnSize(wp, LOWORD(lp), HIWORD(lp));
	//	break;
	//case WM_PAINT:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	pwnd->OnPaint();
	//	break;
	//case WM_COMMAND:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	return pwnd->OnCommand(wp, lp);
	//	break;
	case WM_NCCREATE:
		pwnd = new CMyButton(hWnd);
		pwnd->Register();
		//return TRUE;
		return DefWindowProc(hWnd,msg,wp,lp);
		break;
	case WM_CREATE:
		pwnd = CMyButton::FromHWND(hWnd);
		pwnd->OnCreate((LPCREATESTRUCT)lp);
		break;
	//case WM_MOUSEMOVE:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	pwnd->OnMouseMove((UINT)wp,LOWORD(lp),HIWORD(lp));
	//	break;
	//case WM_LBUTTONDOWN:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	pwnd->OnLButtonDown((UINT)wp,LOWORD(lp),HIWORD(lp));
	//	break;
	//case WM_LBUTTONUP:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	pwnd->OnLButtonUp((UINT)wp,LOWORD(lp),HIWORD(lp));
	//	break;
	//case WM_DROPFILES:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	pwnd->OnDropFiles((HDROP)wp);
	//	break;
	//case WM_TIMER:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	pwnd->OnTimer(wp);
	//	break;
	//case WM_NOTIFY:
	//	pwnd = CMyButton::FromHWND(hWnd);
	//	return pwnd->OnNotify((LPNMHDR)lp);
	//	break;
	case WM_CLOSE:
		pwnd = CMyButton::FromHWND(hWnd);
		pwnd->OnClose();
		break;
	case WM_DESTROY:
		//PostQuitMessage(0);
		break;
	case WM_NCDESTROY:
		pwnd = CMyButton::FromHWND(hWnd);
		delete pwnd;
		break;
	default:
		return DefWindowProc(hWnd,msg,wp,lp);
	}
	return 0;
}

HWND CMyButton::Create(HINSTANCE hInstance, HWND hParent)
{
	WNDCLASSEX wc;
	memset(&wc, 0, sizeof(WNDCLASSEX));
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.hInstance = hInstance;
	wc.style = CS_HREDRAW | CS_VREDRAW /*|CS_DBLCLKS*/;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszMenuName = NULL;
	wc.lpfnWndProc = CMyButton::WndProc;
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	//wc.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1);
	//wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszClassName = (LPCTSTR)CMyButton::szClassName;
	wc.hIcon = NULL;
	wc.hIconSm = NULL;
	RegisterClassEx(&wc);

	HWND hWnd = CreateWindowEx(0/*WS_EX_ACCEPTFILES*/,
		CMyButton::szClassName,
		CMyButton::szTitle, WS_POPUP,
		0, 0, 32+3*2, 32+3*2,
		NULL, NULL, hInstance, NULL);

	return hWnd;
}

#endif

/**
 *
 */
int CButtonBase::SetImage(CImageLib &back, HICON hicon)
{
    if (hicon != NULL) {  // general button
        image_.Create(BUTTON_WIDTH, BUTTON_HEIGHT*2);
        image_.Copy(back, 0, 0,
                    BUTTON_LR_WIDTH, 0,
                    BUTTON_WIDTH, BUTTON_HEIGHT);
        CDibDC dc(image_);
        DrawIconEx(dc, MARGIN_X, MARGIN_Y,
                   hIcon, ICON_WIDTH, ICON_HEIGHT,
                   0, NULL, DI_NORMAL);
        DrawIconEx(dc, MARGIN_X+1, MARGIN_Y+BUTTON_HEIGHT+1,
                   hIcon, ICON_WIDTH, ICON_HEIGHT,
                   0, NULL, DI_NORMAL);
    } else {  // L or R
        image_.Create(BUTTON_LR_WIDTH*2, BUTTON_LR_HEIGHT*2);
        image_.Copy(back, 0, 0,
                    0, 0,
                    BUTTON_LR_WIDTH, BUTTON_HEIGHT);
        image_.Copy(back, BUTTON_LR_WIDTH, 0,
                    BUTTON_LR_WIDTH+BUTTON_WIDTH, 0,
                    BUTTON_LR_WIDTH, BUTTON_HEIGHT);
    }
}


/* - - - - - sandbox - - - - -
	void Copy(CImageLib&image_from,int targetx=0,int targety=0,int srcx=0,int srcy=0,int srcwidth=0,int srcheight=0);

- - - - - sandbox - - - - - */
