/** 
 * @file lunch5_tooltip.h
 * @brief ここに説明を書く
 */
#ifndef __lunch5_TOOLTIP_H__
#define __lunch5_TOOLTIP_H__

#include <windows.h>

#include <cmnstring.h>

#include "lunch5_wndbase.h"

/**
 * ツールチップ表示ウインドウ
 */
class CToolTip : public CWindowBase
{
public:
    CToolTip()
        :CWindowBase(NULL) {}
    explicit CToolTip(HWND hWnd)
        :CWindowBase(hWnd) {}
    ~CToolTip() {}

    void OnTimer(int ID);
    void OnCreate(LPCREATESTRUCT lpcs);
    void OnSize(UINT uFlag, int width, int height);
    void OnGetMinMaxInfo(LPMINMAXINFO pmmi);
    int OnNotify(LPNMHDR pnmhdr);
    void OnClose();
    void OnPaint();

    int OnCommand(WPARAM, LPARAM);

    int show(int param);
    int move(int x, int y);

private:
    CString szTitle;        // 表示する文字列
    COLORREF color_text_;   // 文字の色
    COLORREF color_bkgnd_;  // 背景の色
    int width_;   // 横幅
    int height_;  // 縦幅
    int x_;  // 位置
    int y_;  // 位置

public:
    static CToolTip* FromHWND(HWND hWnd) {
        return reinterpret_cast<CToolTip*>(
            ::GetWindowLongPtr(hWnd, GWLP_USERDATA));
    }
    static HWND Create(HINSTANCE hInstance);
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp);

private:
    static const char* szClassName;
};


#endif  // __lunch5_TOOLTIP_H__
