#include "lunch5_anim.h"

#include <windows.h>

#include <string.h>

#include <fio.h>


// -------------------------------------
// CAnimation
// -------------------------------------

const char* CAnimation::strdrawtbl_[DRAW_SIZE] = {
    "fix",        // ボタンを押した側固定(規定値)
    "left_fix",   // 左側固定
    "shorten",    // 拡大縮小
    "slide",      // ボタンを押していない側固定
    "right_fix",  // 右側固定
};

CAnimation::CAnimation()
{
}

CAnimation::~CAnimation()
{
}

int CAnimation::Open(CStdio *file)
{
    // not yet

    return 0;
}

