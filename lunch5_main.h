#ifndef __lunch5_MAIN_H__
# define __lunch5_MAIN_H__


//------------------------------------------------
// 定数
//------------------------------------------------

//------------------------------------------------
// 変数
//------------------------------------------------

class CLunch5App
{
public:
	CLunch5App(){}

	int InitFirst();
	void Final();

	int InitInstance(HINSTANCE hInstance, int nShowCmd);

	HINSTANCE hInst_;
	HWND hMainWnd_;
	HANDLE hMutex_;
};



//------------------------------------------------
// 関数
//------------------------------------------------

#define TOGGLE_BIT(param, bit) (((param)&(~(bit)))|(((param)&(bit))^(bit)))
#define CHECK_BIT(param, bit) (((param)&(bit)) != 0)
#define CLEAR_BIT(param, bit) ((param)&(bit))


#endif  // __lunch5_MAIN_H__
