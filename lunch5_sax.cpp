#include "lunch5_sax.h"

#include <windows.h>

#include <string.h>

#include <sax.h>

#include "lunch5_item.h"


// -------------------------------------
// CLunch5Sax
// -------------------------------------

const char *CLunch5Sax::tags_[] = {
    "",  // STAGE_NONE = 0,

    "LUNCH5",  // STAGE_LUNCH,

    "ANIMATION",  // STAGE_ANIM,
    "MOVE",       // STAGE_ANIM_MOVE,
    "DRAW",       // STAGE_ANIM_DRAW,

    "BUTTON",   // STAGE_BUTTON,
    "CMDLINE",  // STAGE_BUTTON_CMDLINE,
    "DIR",      // STAGE_BUTTON_DIR,
    "FACE",     // STAGE_BUTTON_FACE,
    "ICON",     // STAGE_BUTTON_ICON,
    "PATH",     // STAGE_BUTTON_PATH,

    "CONFIGURATION",  // STAGE_CONFIG,
    "CAPTION",        // STAGE_CONFIG_CAPTION,
    "CHECKDISK",      // STAGE_CONFIG_CHECKDISK,
    "PASSWORD",       // STAGE_CONFIG_PASSWD,
    "PRIORITY",       // STAGE_CONFIG_PRIORITY,
    "TRAYTEXT",       // STAGE_CONFIG_TRAYTEXT,

    "WND",    // STAGE_WND,
    "HOVER",  // STAGE_WND_HOVER,
    "LEFT",   // STAGE_WND_LEFT,
    "RIGHT",  // STAGE_WND_RIGHT,
    "SKIN",   // STAGE_WND_SKIN,
    "X",      // STAGE_WND_X,
    "Y",      // STAGE_WND_Y,

    "",  // STAGE_MAX,
};

int CLunch5Sax::elementStart(const char *name,
                                     CAttribArray &atrary)
{
    int i;
    int ntag;
    CAttrib *pa;

    for (int ntag = 0; ntag < STAGE_MAX; ++ntag) {
        if (lstrcmpi(name, tags_[ntag]) == 0) {
            break;
        }
    }
    if (ntag == STAGE_MAX) {
        // unknown tag.
        return 0;
    }

    switch (ntag) {
    case STAGE_LUNCH:
        if (nstage_ == STAGE_NONE) {
            nstage_ = ntag;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_ANIM:
        if (nstage_ == STAGE_LUNCH) {
            nstage_ = ntag;
            for (i = 0 ; i < atrary.Size() ; ++i) {
                pa = atrary.Get(i);
                if (lstrcmpi(pa->GetName(), tags_[STAGE_ANIM_MOVE]) == 0) {
                    // pconfig_->;
                } else if (
                    lstrcmpi(pa->GetName(), tags_[STAGE_ANIM_DRAW]) == 0) {
                    // pconfig_->;
                }
            }
        } else {
            // invalid xml.
        }
        break;
    case STAGE_ANIM_MOVE:
        if (nstage_ == STAGE_ANIM) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_ANIM_DRAW:
        if (nstage_ == STAGE_ANIM) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;

    case STAGE_BUTTON:
        if (nstage_ == STAGE_LUNCH) {
            nstage_ = ntag;
            // pconfig_->;
            if (pitem_ == NULL) {
            } else {
                delete pitem_;
            }
            pitem_ = new CItem();
            // pitem_->Init();
        } else {
            // invalid xml.
        }
        break;
    case STAGE_BUTTON_CMDLINE:
        if (nstage_ == STAGE_BUTTON) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_BUTTON_DIR:
        if (nstage_ == STAGE_BUTTON) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_BUTTON_FACE:
        if (nstage_ == STAGE_BUTTON) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_BUTTON_ICON:
        if (nstage_ == STAGE_BUTTON) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_BUTTON_PATH:
        if (nstage_ == STAGE_BUTTON) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;

    case STAGE_CONFIG:
        if (nstage_ == STAGE_LUNCH) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_CONFIG_CAPTION:
        if (nstage_ == STAGE_CONFIG) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_CONFIG_CHECKDISK:
        if (nstage_ == STAGE_CONFIG) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_CONFIG_PASSWD:
        if (nstage_ == STAGE_CONFIG) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_CONFIG_PRIORITY:
        if (nstage_ == STAGE_CONFIG) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_CONFIG_TRAYTEXT:
        if (nstage_ == STAGE_CONFIG) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;

    case STAGE_WND:
        if (nstage_ == STAGE_LUNCH) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_WND_HOVER:
        if (nstage_ == STAGE_WND) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_WND_LEFT:
        if (nstage_ == STAGE_WND) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_WND_RIGHT:
        if (nstage_ == STAGE_WND) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_WND_SKIN:
        if (nstage_ == STAGE_WND) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_WND_X:
        if (nstage_ == STAGE_WND) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;
    case STAGE_WND_Y:
        if (nstage_ == STAGE_WND) {
            nstage_ = ntag;
            // pconfig_->;
        } else {
            // invalid xml.
        }
        break;

    default:
    case STAGE_NONE:
        break;
    }
    return 0;
}

int CLunch5Sax::elementEnd(const char *name)
{
    int i;
    int ntag;

    for (int ntag = 0; ntag < STAGE_MAX; ++ntag) {
        if (lstrcmpi(name, tags_[ntag]) == 0) {
            break;
        }
    }
    if (ntag == STAGE_MAX) {
        // unknown tag.
        return 0;
    }

    if (ntag != nstage_) {
        // invalid xml.
    }
    switch (ntag) {
    case STAGE_LUNCH:
        nstage_ = STAGE_NONE;
        break;

    case STAGE_ANIM:
    case STAGE_CONFIG:
    case STAGE_WND:
        nstage_ = STAGE_LUNCH;
        break;

    case STAGE_BUTTON:
        nstage_ = STAGE_LUNCH;
        if (pitem_ != NULL) {
            pconfig_->AddItem(pitem_);
            delete pitem_;
            pitem_ = NULL;
        }
        break;

    case STAGE_ANIM_MOVE:
    case STAGE_ANIM_DRAW:
        nstage_ = STAGE_ANIM;
        break;

    case STAGE_BUTTON_CMDLINE:
    case STAGE_BUTTON_DIR:
    case STAGE_BUTTON_FACE:
    case STAGE_BUTTON_ICON:
    case STAGE_BUTTON_PATH:
        nstage_ = STAGE_BUTTON;
        break;

    case STAGE_CONFIG_CAPTION:
    case STAGE_CONFIG_CHECKDISK:
    case STAGE_CONFIG_PASSWD:
    case STAGE_CONFIG_PRIORITY:
    case STAGE_CONFIG_TRAYTEXT:
        nstage_ = STAGE_CONFIG;
        break;

    case STAGE_WND_HOVER:
    case STAGE_WND_LEFT:
    case STAGE_WND_RIGHT:
    case STAGE_WND_SKIN:
    case STAGE_WND_X:
    case STAGE_WND_Y:
        nstage_ = STAGE_WND;
        break;

    default:
    case STAGE_NONE:
        break;
    }
    return 0;
}

int CLunch5Sax::characters(const char *str)
{
    switch (nstage_) {
    case STAGE_ANIM_MOVE:
        // stage_ = STAGE_ANIM;
        // 名前だけセットする
        pconfig_->AnimMoveType(str);
        break;
    case STAGE_ANIM_DRAW:
        // stage_ = STAGE_ANIM;
        // fix       : ボタンを押した側固定(規定値)
        // left_fix  : 左側固定
        // shorten   : 拡大縮小
        // slide     : ボタンを押していない側固定
        // right_fix : 右側固定
        pconfig_->AnimDrawType(str);
        break;

    case STAGE_BUTTON_CMDLINE:
        if (pitem_ != NULL) {
            pitem_->SetCmdline(str);
        }
        break;
    case STAGE_BUTTON_DIR:
        if (pitem_ != NULL) {
            pitem_->SetDirectory(str);
        }
        break;
    case STAGE_BUTTON_FACE:
        if (pitem_ != NULL) {
            pitem_->SetFaceText(str);
        }
        break;
    case STAGE_BUTTON_ICON:
        if (pitem_ != NULL) {
            pitem_->SetIconPath(str);
        }
        break;
    case STAGE_BUTTON_PATH:
        if (pitem_ != NULL) {
            pitem_->SetCmdPath(str);
        }
        break;
    case STAGE_CONFIG_CHECKDISK:
        // stage_ = STAGE_CONFIG;
        // 0:チェックしない(規定値)
        // 1:チェックする
        pconfig_->SetCheckDisk(atoi(str) == 1);
        break;

    case STAGE_CONFIG_CAPTION:
    case STAGE_CONFIG_PASSWD:
    case STAGE_CONFIG_PRIORITY:
        // stage_ = STAGE_CONFIG;
        break;
    case STAGE_CONFIG_TRAYTEXT:
        pconfig_->SetTrayText(str);
        // stage_ = STAGE_CONFIG;
        break;

    case STAGE_WND_HOVER:
        // stage_ = STAGE_WND;
        break;

    case STAGE_WND_LEFT:
        // type of LEFT-button
        // disable : 押下不可
        // visible : 押下可能(既定値)
        // enable  : 押下可能(既定値)
        // hide    : 隱す
        // menu    : メニューを表示(未実装)
        // stage_ = STAGE_WND;
        pconfig_->SetLeftBtnState(CLunch5Config::BtnStateFromStr(str));
        break;
    case STAGE_WND_RIGHT:
        // type of RIGHT-button
        // disable : 押下不可
        // visible : 押下可能(既定値)
        // enable  : 押下可能(既定値)
        // hide    : 隱す
        // menu    : メニューを表示(未実装)
        // stage_ = STAGE_WND;
        pconfig_->SetRightBtnState(CLunch5Config::BtnStateFromStr(str));
        break;
    case STAGE_WND_SKIN:
        // filename of skin
        // stage_ = STAGE_WND;
        pconfig_->SetSkinName(str);
        break;
    case STAGE_WND_X:
        // x-pos or RIGHT
        // stage_ = STAGE_WND;
        pconfig_->SetPosX(str);
        break;
    case STAGE_WND_Y:
        // y-pos or BOTTOM
        // stage_ = STAGE_WND;
        pconfig_->SetPosY(str);
        break;

    default:
    case STAGE_NONE:
    case STAGE_LUNCH:
    case STAGE_ANIM:
    case STAGE_BUTTON:
    case STAGE_CONFIG:
    case STAGE_WND:
        break;
    }
    return 0;
}
