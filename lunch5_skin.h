#ifndef __LUNCH5_SKIN_H__
#define __LUNCH5_SKIN_H__

#include <windows.h>

#include <Shlwapi.h>

#include <cmnstring.h>

#include <fio.h>
class CImageLib;
//#include <imagelib.h>


class CSkin
{
public:
    CSkin();
    virtual ~CSkin();

    int Open(CStdio *file);

    int LoadDIM(CImageLib &image, CStdio *file);

    void WriteXML(CStdio *file) {
        char buf[MAX_PATH];
        strcpy(buf, path_);
        PathStripPath(buf);
        file->Print("  <skin>%s</skin>\n", buf);
    }
    void SetPath(const char *path) {
        path_ = path;
    }
private:
    CString path_;
    CImageLib image_;
};


#endif  // __LUNCH5_SKIN_H__
