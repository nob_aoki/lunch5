/** 
 * @file lunch5_wnd.h
 * @brief ここに説明を書く
 */
#ifndef __lunch5_WND_H__
#define __lunch5_WND_H__

#include "lunch5_wndbase.h"

class CMainWindow : public CWindowBase
{
public:
	enum {
		FIXCORNER_LEFTTOP = 0,
		FIXCORNER_RIGHTTOP,
		FIXCORNER_LEFTBOTTOM,
		FIXCORNER_RIGHTBOTTOM,
	};
public:
	CMainWindow()
		:CWindowBase(NULL) {}
	CMainWindow(HWND hWnd)
		:CWindowBase(hWnd) {}
	~CMainWindow() {}

	void OnTimer(int ID);
	void OnCreate(LPCREATESTRUCT lpcs);
	void OnSize(UINT uFlag, int width, int height);
	void OnGetMinMaxInfo(LPMINMAXINFO pmmi);
	void OnDropFiles(HDROP hDrop);
	int OnNotify(LPNMHDR pnmhdr);
	void OnClose();
	void OnPaint();

	int OnCommand(WPARAM, LPARAM);
	void OnOpen();
	void OnAbout();
	void OnHelp();

	void Resize(int width, int height, int type);

private:
	

public:
	static CMainWindow*FromHWND(HWND hWnd) {
		return reinterpret_cast<CMainWindow*>(
			::GetWindowLongPtr(hWnd, GWLP_USERDATA)
		);
	}
	static HWND Create(HINSTANCE hInstance);
	static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp);

protected:


private:
	static const char* szClassName;
	static const char* szTitle;
};


#endif //__lunch5_WND_H__
