#ifndef __LUNCH5_CONFIG_H__
#define __LUNCH5_CONFIG_H__

#include <windows.h>

#include <cmnstring.h>
#include <fio.h>

#include "lunch5_anim.h"
#include "lunch5_item.h"
#include "lunch5_skin.h"



class CLunch5Config
{
public:
    enum {
        ALIGN_TOP,
        ALIGN_BOTTOM,
        ALIGN_DEFAULT = ALIGN_TOP,

        BTN_DISABLED,
        BTN_HIDDEN,
        BTN_ENABLED,

        CAP_FG_COLOR = 0xffffffu,
        CAP_BG_COLOR = 0xff2010u,

        HOVER_ACTIVE_TIME = 2000,
    };
public:
    CLunch5Config();
    ~CLunch5Config();

    void Init() {
        caption_align_ = ALIGN_DEFAULT;  // キャプションを表示する位置
        caption_fg_color_ = CAP_FG_COLOR;  // キャプションの文字の色
        caption_bg_color_ = CAP_BG_COLOR;  // キャプションの背景色
        checkdisk_ = 0;  // シャットダウン時に
                         // リムーバブルディスクの抜き忘れを
                         // チェックするかどうか
        priority_ = NORMAL_PRIORITY_CLASS;   // プロセスの優先度
        traytext_ = "Lunch5";  // トレイにマウスを持っていった時に表示する文字

        right_button_ = BTN_ENABLED;
        left_button_ = BTN_ENABLED;

        hover_caption_ = 1;
        hover_activate_ = 1;
        hover_act_time_ = HOVER_ACTIVE_TIME;
    }

    int Open(CStdio*file);
    int Save(CStdio*file);

    void SetCaptionAlign(int align) {caption_align_ = align;}
    void SetCaptionFGColor(COLORREF color) {caption_fg_color_ = color;}
    void SetCaptionBGColor(COLORREF color) {caption_bg_color_ = color;}
    void SetCheckDisk(int b) {checkdisk_ = b;}
    void SetPriority(int pri) {priority_ = pri;}
    void SetTrayText(const char* text) {traytext_ = text;}
    void SetLeftBtnState(int st) {left_button_ = st;}
    void SetRightBtnState(int st) {right_button_ = st;}
    void SetPosX(const char *x) {
        if (stricmp(x, "right") == 0) {
            pos_x_ = x;
        } else {
            char *p;
            int val = strtol(x, &p, 10);
            if (p == '\0') {
                pos_x_ = x;
            } else {
                // アルファベットを含むときはエラー
            }
        }
    }
    void SetPosY(const char *y) {
        if (stricmp(y, "bottom") == 0) {
            pos_y_ = y;
        } else {
            char *p;
            int val = strtol(y, &p, 10);
            if (p == '\0') {
                pos_y_ = y;
            } else {
                // アルファベットを含むときはエラー
            }
        }
    }

    int GetCaptionAlign() {return caption_align_;}
    COLORREF GetCaptionFGColor() {return caption_fg_color_;}
    COLORREF GetCaptionBGColor() {return caption_bg_color_;}
    int GetCheckDisk() {return checkdisk_;}
    int GetPriority() {return priority_;}
    const char* SetTrayText() {return traytext_;}
    int GetLeftBtnState() {return left_button_;}
    int GetRightBtnState() {return right_button_;}

    void AddItem(CItem*pitem) {
        itemarray_.Add(pitem);
    }

    int AnimDrawType(const char* str) {
        int type = CAnimation::DrawTypeFromString(str);
        anim_.SetDrawType(type);
        return type;
    }
    void AnimMoveType(const char* str) {
        anim_.SetMoveType(str);
    }
    void SetSkinName(const char*str) {
        skin_.SetPath(str);
    }
private:
    CItemArray itemarray_;  // ボタンの情報
    CSkin skin_;  // スキンのデータ
    CAnimation anim_;  // アニメーション関係の設定

    int caption_align_;  // キャプションを表示する位置
    COLORREF caption_fg_color_;  // キャプションの文字の色
    COLORREF caption_bg_color_;  // キャプションの背景色
    int checkdisk_;  // シャットダウン時に
                     // リムーバブルディスクの抜き忘れを
                     // チェックするかどうか
    DWORD priority_;   // プロセスの優先度
    CString traytext_;  // トレイにマウスを持っていった時に表示する文字

    CString pos_x_;
    CString pos_y_;

    int right_button_;  // 右の矢印ボタンを表示するかどうか
    int left_button_;  // 左の矢印ボタンを表示するかどうか

    int hover_caption_;   // キャプションを表示するかどうか
    int hover_activate_;  // マウスを置きっぱなしにして前面に持ってくるかどうか
    int hover_act_time_;  // 前面に持ってくると判断するまでの時間[msec]

    int must_be_in_desktop_;  // デスクトップの外に0:出てもいい、1:ダメ
    int closed_window_;  // ランチャーが開いてるかどうか 0:開、1:閉。
    int warn_slight_move_;  // 数ドット移動した時に警告を出すかどうか。
    int fix_position_;  // ランチャーのD&Dを0:許可する、1:ダメ

public:
    static COLORREF SwapRB(COLORREF c) {
        return ((c&0xffu) << 16) | ((c&0xff00u)) | ((c&0xff0000u) >> 16);
    }
    static int BtnStateFromStr(const char* str);
};


#endif  // __LUNCH5_CONFIG_H__
