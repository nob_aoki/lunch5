/** 
 * @file lunch5_splashscreen.cpp
 * @brief ここに説明を書く
 * 
 */

#include "lunch5_splashscreen.h"

#include <windows.h>

#include <Shlwapi.h>

#include <ImageLib.h>

#include "version.h"


#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif


CSplashScreenWnd*gSplashWnd;
//CSplashScreenWnd gSplashWnd;

const char*CSplashScreenWnd::szClassName = "lunch5_splashscrn";


HWND CSplashScreenWnd::Create(HWND hParent, HINSTANCE hInst)
{
	WNDCLASSEX wc;
	memset(&wc, 0, sizeof(WNDCLASSEX));
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.hInstance = hInst;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hCursor = LoadCursor(NULL, IDC_WAIT);
	wc.lpszMenuName = NULL;
	wc.lpfnWndProc = (WNDPROC)CSplashScreenWnd::WndProc;
	wc.hbrBackground = NULL;
	wc.lpszClassName = (LPCTSTR)CSplashScreenWnd::szClassName;
	RegisterClassEx(&wc);

	HWND hWnd = CreateWindowEx(WS_EX_TOPMOST,
		CSplashScreenWnd::szClassName,
		"",
		WS_POPUP|WS_BORDER|WS_VISIBLE,
		0, 0, 3, 3,
		hParent, NULL, hInst, NULL);

	::ShowWindow(hWnd, SW_SHOWNORMAL);

	UpdateWindow(hWnd);

	return hWnd;
}

void CSplashScreenWnd::OnCreate()
{
	//HWND hProgress_=CreateWindowEx(0,PROGRESS_CLASS,"",WS_VISIBLE|WS_CHILD|PBS_MARQUEE
	//			,0,0,1,1,hWnd_,HMENU(1),GetModuleHandle(NULL),NULL);
	//SendMessage(hProgress_,PBM_SETMARQUEE,1,10);
	char buf[1024];
	GetModuleFileName(NULL,buf,sizeof(buf));
	PathRemoveFileSpec(buf);
	strcat(buf,"\\rsc\\splash.png");
	image_.LoadPng(buf);
	int x = GetSystemMetrics(SM_CXSCREEN);
	int y = GetSystemMetrics(SM_CYSCREEN);
	x -= image_.Width()+2;
	y -= image_.Height()+2/*+10*/;
	x /= 2;
	y /= 2;
	SetWindowPos(NULL,x,y,image_.Width()+2,image_.Height()+2/*+10*/,SWP_NOZORDER);
	//SetWindowPos(hProgress_,NULL,0,image_.Height(),image_.Width()+2,10,SWP_NOZORDER);
}

void CSplashScreenWnd::OnPaint(HDC hdc)
{
	if(image_.IsOK())
		image_.Draw(hdc);

	SelectObject(hdc,GetStockObject(DEFAULT_GUI_FONT));

	RECT r = {0, 0, 0, 0};
	char buf[40] = "ver.";
	strcat(buf, LUNCH5_VERSION_TEXT);
	DrawText(hdc, buf, strlen(buf), &r, DT_CALCRECT|DT_SINGLELINE);
	RECT rc;
	GetClientRect(&rc);
	r.left = rc.right - r.right;
	r.right = rc.right;
	r.top = rc.bottom - r.bottom;
	r.bottom = rc.bottom;
	DrawText(hdc, buf, strlen(buf), &r, DT_SINGLELINE);
}

LRESULT CSplashScreenWnd::WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
	CSplashScreenWnd* pwnd;
	PAINTSTRUCT ps;
	switch (msg) {
	case WM_NCCREATE:
		pwnd = new CSplashScreenWnd(hWnd);
		pwnd->Register();
		return TRUE;
		break;
	case WM_NCDESTROY:
		pwnd = CSplashScreenWnd::FromHWND(hWnd);
		delete pwnd;
		break;
	case WM_CREATE:
		pwnd = CSplashScreenWnd::FromHWND(hWnd);
		pwnd->OnCreate();
		break;
	case WM_PAINT:
		pwnd = CSplashScreenWnd::FromHWND(hWnd);
		pwnd->OnPaint(BeginPaint(hWnd, &ps));
		EndPaint(hWnd, &ps);
		break;
	//case WM_COMMAND:
	//	break;
	//case WM_LBUTTONDBLCLK:
	//	break;
	case WM_CLOSE:
		pwnd = CSplashScreenWnd::FromHWND(hWnd);
		pwnd->DestroyWindow();
		break;
    default:
		return DefWindowProc(hWnd, msg, wp, lp);
    }
    return 0;
}



/* sandbox


sandbox */