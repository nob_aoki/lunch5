/** 
 * @file roctia_wnd.cpp
 * @brief ここに説明を書く
 * 
 * @note 2011.09.20 (株)トーメーコーポレーション  青木 伸頼 作成
 */

#include "roctia_wnd.h"

#include <windows.h>

#include <cstdio>
#include <cmath>

#include <commctrl.h>

#include <wndutil.h>
#include <imagelib.h>
#include <fio.h>

#include "resource.h"
#include "roctia_wndmsg.h"

#include "roctia_prgs.h"
#include "roctia_sharemem.h"
#include "inifile.h"
#include "roctia_logwnd.h"

#include "roctia_rpanemgr.h"

#include "roctia_patientinfownd.h"
#include "roctia_pictctrltabwnd.h"
#include "roctia_splashscreen.h"

#include "roctia_toolbar.h"

#include "roctia_main.h"


#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif

//------------------------------------------------
// 定数
//------------------------------------------------

const char* CMainWindow::szClassName = "roctia_mainwindow";
const char* CMainWindow::szTitle = "Retinal OCT Image Analyzer";

//------------------------------------------------
// 変数
//------------------------------------------------




void CMainWindow::OnTimer(int ID)
{
	KillTimer(hWnd_, 1);
	gSplashWnd->Close();
	EnableWindow(hWnd_, TRUE);
}

/**
 * ウインドウの初期化
 */
void CMainWindow::OnCreate(LPCREATESTRUCT lpcs)
{
	EnableWindow(hWnd_, FALSE);

	//HWND hItem;
	int width, tbheight;
	//char buf[MAX_PATH*2];  // ,*p;
	RECT rc, rc1, rc2, rc3, rc4;  // ,rc5;

	CenterWindow(GetDesktopWindow(), hWnd_);

	gPrgsWnd.Create(hWnd_, hInst, (WNDPROC)ProgressWndProc);

//	gLogWnd.Create(hWnd, hInst);
	gInifile->Dump();

	GetClientRect(hWnd_, &rc1);

	/* toolbar ------>> */
	toolbar_.Create(hInst, hWnd_);
	tbheight = toolbar_.GetHeight();
	/* <<------ toolbar */

	hPatientInfoWnd_ = CPatientInfoWnd::CreatePane(hWnd_);
	GetWindowRect(hPatientInfoWnd_, &rc2);

	//hRPaneWnd = CRPaneWnd::CreatePane(hWnd);
	gRpanemgr = new CRPaneManager;
	gRpanemgr->Init(hWnd_);
	GetWindowRect(gRpanemgr->GetActivePane(), &rc4);

	HWND hWnd = CPictCtrlTab::CreatePane(hWnd_);
	pPictCtrlTabWnd_ = CPictCtrlTab::FromHWND(hWnd);

	GetWindowRect(hWnd, &rc3);

	if (rc2.right-rc2.left > rc3.right-rc3.left) {
		width = rc2.right-rc2.left;
	} else {
		width = rc3.right-rc3.left;
	}

	rc.left = 0;
	rc.top = 0;
	rc.right = width+rc4.right-rc4.left;
	rc.bottom = rc2.bottom-rc2.top+rc3.bottom-rc3.top+tbheight;
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, TRUE);
	minimumsize_[0] = rc.right-rc.left;
	if(minimumsize_[0] > GetSystemMetrics(SM_CXSCREEN))
		minimumsize_[0] = GetSystemMetrics(SM_CXSCREEN);
	minimumsize_[1] = rc.bottom-rc.top;
	if(minimumsize_[1] > GetSystemMetrics(SM_CYSCREEN))
		minimumsize_[1] = GetSystemMetrics(SM_CYSCREEN);

	SetWindowPos(hPatientInfoWnd_, NULL, 0, tbheight,
		width, rc2.bottom-rc2.top, SWP_NOZORDER);
	SetWindowPos(pPictCtrlTabWnd_->GetHWND(), NULL, 0, rc2.bottom-rc2.top+tbheight,
		width, rc1.bottom-rc1.top-(rc2.bottom-rc2.top)-tbheight, SWP_NOZORDER);
	SetWindowPos(gRpanemgr->GetActivePane(), NULL, rc3.right-rc3.left, tbheight,
		rc1.right-rc1.left-width, rc1.bottom-rc1.top-tbheight, SWP_NOZORDER);
	SetWindowPos(hWnd_,NULL,0,0,
		(rc1.right>minimumsize_[0])?rc1.right:minimumsize_[0],
		(rc1.right>minimumsize_[1])?rc1.bottom:minimumsize_[1],SWP_NOZORDER|SWP_NOMOVE);

	if (__argc > 1) {
		OnTimer(1);  // EnableWindow(hWnd,TRUE);
		if (OpenFile(__argv[1])) {
			MessageBox(hWnd_, "startup option error", __argv[1], MB_ICONWARNING);
		}
	} else {
		int ret, d;
		ret = gInifile->Get("Misc", "SplashScreen", d);
		if ( (ret != 0) && (d == 0) ) {
			OnTimer(1);  // EnableWindow(hWnd,TRUE);
		} else {
			SetTimer(hWnd_, 1, 1000, NULL);
		}
	}
}

void CMainWindow::OnSize(UINT/*uFlag*/, int/* width*/, int/* height*/)
{
	int width_left, tbheight;
	RECT rc1, rc2, rc3;  // ,rc4,rc5;

	GetClientRect(hWnd_, &rc1);
	GetWindowRect(hPatientInfoWnd_, &rc2);
	GetWindowRect(pPictCtrlTabWnd_->GetHWND(), &rc3);
	// GetWindowRect(hgRpanemgr->GetActivePane(), &rc4);
	// GetWindowRect(hToolBar, &rc5);

	// toolbar.OnSize(uFlag, MAKELPARAM(width, height));
	tbheight = toolbar_.GetHeight();  // rc5.bottom-rc5.top;

	if (rc2.right-rc2.left > rc3.right-rc3.left) {
		width_left = rc2.right-rc2.left;
	} else {
		width_left = rc3.right-rc3.left;
	}

	SetWindowPos(hPatientInfoWnd_, NULL, 0, tbheight,
		width_left, rc2.bottom-rc2.top, SWP_NOZORDER);
	SetWindowPos(pPictCtrlTabWnd_->GetHWND(), NULL, 0, tbheight+rc2.bottom-rc2.top,
		width_left, rc1.bottom-rc1.top-(rc2.bottom-rc2.top)-tbheight, SWP_NOZORDER);

	SetWindowPos(gRpanemgr->GetActivePane(), NULL, rc3.right-rc3.left, tbheight,
		rc1.right-rc1.left-width_left, rc1.bottom-rc1.top-tbheight, SWP_NOZORDER);
}

void CMainWindow::OnGetMinMaxInfo(LPMINMAXINFO pmmi)
{
	// RECT rc1,rc2,rc3;
	pmmi->ptMinTrackSize.x = minimumsize_[0];
	pmmi->ptMinTrackSize.y = minimumsize_[1];
}

void CMainWindow::OnDropFiles(HDROP hDrop)
{
	char filename[MAX_PATH];

	DragQueryFile(hDrop, 0, filename, sizeof(filename));
	DragFinish(hDrop);

	OpenFile(filename);
}

void CMainWindow::OnAckOpen(COPYDATASTRUCT *pcd)
{
	// フォアグラウンドウインドウにする。
#if 1  // 行けそう。
	SetWindowPos(hWnd_, HWND_TOPMOST, 0, 0, 0, 0,
		SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
	SetWindowPos(hWnd_, HWND_NOTOPMOST, 0, 0, 0, 0,
		SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
#endif
#if 0  // 効果なし
	ShowWindow(hWnd_, SW_SHOWNORMAL);
	SetForegroundWindow(hWnd);
#endif

	if (MessageBox(hWnd_, "Received a new file.\nOpen it?",
		(const char*)pcd->lpData, MB_OKCANCEL) != IDOK) {
		return;
	}
	OpenFile((const char*)pcd->lpData);
}

int CMainWindow::OnNotify(LPNMHDR pnmhdr)
{
	if (pnmhdr->hwndFrom == toolbar_.GetTBHWND()) {
		toolbar_.OnNotifyTB((LPNMTOOLBAR)pnmhdr);
	} else if (pnmhdr->hwndFrom == toolbar_.GetRBHWND()) {
		toolbar_.OnNotifyRB(pnmhdr);
	} else {
	}
	return 0;
}

void CMainWindow::OnClose()
{
	gRpanemgr->Destroy();
	pPictCtrlTabWnd_->Destroy();
	toolbar_.Destroy();
//	if (hToolBar)
//		DestroyWindow(hToolBar);
//	if (hImageList)
//		ImageList_Destroy(hImageList);
	gPrgsWnd.Close();
	gLogWnd.OnClose();
	DestroyWindow(hWnd_);
}

/**
*	MainDlgProc
*/
LRESULT CALLBACK CMainWindow::WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
	CMainWindow*pwnd;
	switch (msg) {
	case WM_SIZE:
		pwnd = CMainWindow::FromHWND(hWnd);
		pwnd->toolbar_.OnSize(wp, lp);
		// SendMessage(hToolBar, WM_SIZE, wp, lp);
		pwnd->OnSize(wp, LOWORD(lp), HIWORD(lp));
		break;
	case WM_COMMAND:
		pwnd = CMainWindow::FromHWND(hWnd);
		return pwnd->OnCommand(wp, lp);
		break;
	case WM_NCCREATE:
		pwnd = new CMainWindow(hWnd);
		pwnd->Register();
		//return TRUE;
		return DefWindowProc(hWnd,msg,wp,lp);
		break;
	case WM_CREATE:
		pwnd = CMainWindow::FromHWND(hWnd);
		pwnd->OnCreate((LPCREATESTRUCT)lp);
		break;
	case WM_GETMINMAXINFO:
		pwnd = CMainWindow::FromHWND(hWnd);
		if(pwnd)
			pwnd->OnGetMinMaxInfo((LPMINMAXINFO)lp);
		break;
	case WM_DROPFILES:
		pwnd = CMainWindow::FromHWND(hWnd);
		pwnd->OnDropFiles((HDROP)wp);
		break;
	case WM_TIMER:
		pwnd = CMainWindow::FromHWND(hWnd);
		pwnd->OnTimer(wp);
		break;
	case WM_RPANE_INITRENDER:
		PostMessage(gRpanemgr->GetActivePane(), msg, wp, lp);
		break;
	case WM_NOTIFY:
		pwnd = CMainWindow::FromHWND(hWnd);
		return pwnd->OnNotify((LPNMHDR)lp);
		break;
	case WM_COPYDATA:
		pwnd = CMainWindow::FromHWND(hWnd);
		pwnd->OnAckOpen((COPYDATASTRUCT*)lp);
		return TRUE;
		break;
	case WM_CLOSE:
		pwnd = CMainWindow::FromHWND(hWnd);
		pwnd->OnClose();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_NCDESTROY:
		pwnd = CMainWindow::FromHWND(hWnd);
		delete pwnd;
		break;
	default:
		return DefWindowProc(hWnd,msg,wp,lp);
	}
	return 0;
}

HWND CMainWindow::Create(HINSTANCE hInstance)
{
	WNDCLASSEX wc;
	memset(&wc, 0, sizeof(WNDCLASSEX));
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.hInstance = hInstance;
	wc.style = 0;//CS_HREDRAW | CS_VREDRAW /*|CS_DBLCLKS*/;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.lpszMenuName = NULL;
	wc.lpfnWndProc = CMainWindow::WndProc;
	//wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	//wc.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1);
	wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszClassName = (LPCTSTR)CMainWindow::szClassName;
	wc.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1));
	wc.hIconSm = (HICON)LoadImage(hInst, MAKEINTRESOURCE(IDI_ICON1),
			IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
	RegisterClassEx(&wc);

	HWND hWnd = CreateWindowEx(WS_EX_ACCEPTFILES, CMainWindow::szClassName,
		CMainWindow::szTitle, WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
		NULL, NULL, hInstance, NULL);

	return hWnd;
}

void CMainWindow::SendCopyData(COPYDATASTRUCT*pcd)
{
	HWND hWnd = FindWindow(CMainWindow::szClassName, CMainWindow::szTitle);
	if (hWnd == NULL) {
	} else {
		SendMessage(hWnd, WM_COPYDATA, NULL, (LPARAM)pcd);
	}
}

/* - - - - - sandbox - - - - -

- - - - - sandbox - - - - - */
 