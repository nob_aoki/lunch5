#include <windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <commctrl.h>
#include <Shlwapi.h>

#include <wndutil.h>
#include <imagelib.h>
#include <fio.h>

#include "lunch5_wnd.h"

#include "resource.h"

#include "lunch5_main.h"


#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif

//------------------------------------------------
// 定数
//------------------------------------------------


//------------------------------------------------
// 変数
//------------------------------------------------


//HWND hMainWnd;
//HANDLE hMutex;
//HINSTANCE hInst;

CLunch5App theApp;

int CLunch5App::InitInstance(HINSTANCE hInstance, int nShowCmd)
{
	hMainWnd_ = CMainWindow::Create(hInstance);

	ShowWindow(hMainWnd_, nShowCmd);
	UpdateWindow(hMainWnd_);

	return 0;
}

/**
 * ウインドウを出す前にやること
 */
int CLunch5App::InitFirst()
{
	hMutex_ = CreateMutex(NULL, TRUE, "LUNCH5_MUTEX_OBJECT");
	if (hMutex_ == NULL) {
		MessageBox(NULL, 
			"Failed CreateMutex().\nPleaseRestart this program.",
			NULL, MB_ICONSTOP);
		return 1;
	}
	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		CloseHandle(hMutex_);
		MessageBox(NULL,"this software is limited to a single instance."
			/*"このソフトウェアは二重に起動することが出来ません。"*/,
			"instance limitation error", MB_ICONWARNING);
		return 1;
	}

	// スクリーンセーバーを無効にする
	// ::SystemParametersInfo( SPI_SETSCREENSAVEACTIVE, FALSE, 0, 0);

//	HWND hWnd = CSplashScreenWnd::Create(NULL, hInst);
//	gSplashWnd = CSplashScreenWnd::FromHWND(hWnd);

#ifdef _WIN64
#else
	_set_SSE2_enable(1);  // SSE2命令の数学関数を1:使う0:使わない。
#endif

	CoInitialize(0);

	INITCOMMONCONTROLSEX ic;
	ic.dwSize = sizeof(INITCOMMONCONTROLSEX);
	ic.dwICC = ICC_WIN95_CLASSES | ICC_BAR_CLASSES | ICC_COOL_CLASSES;
	InitCommonControlsEx(&ic);

	timeBeginPeriod(5);

	return 0;
}

/**
 * ウインドウが消えてからやること
 */
void CLunch5App::Final()
{
	// スクリーンセーバーを有効にする
	// ::SystemParametersInfo( SPI_SETSCREENSAVEACTIVE, TRUE, 0, 0);

	CoUninitialize();

	CloseHandle(hMutex_);

	timeEndPeriod(5);

	// この時点で開放されていないメモリの情報の表示(_DEBUGのときだけ)
	//_CrtDumpMemoryLeaks();
}

/**
*	Entrypoint of this program
*/
int WINAPI WinMain(HINSTANCE hInstance,
		HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF|_CRTDBG_LEAK_CHECK_DF);

	// hInst = hInstance;

	if (theApp.InitFirst())
		return 1;

	if (theApp.InitInstance(hInstance, nShowCmd))
		return 2;

	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	theApp.Final();

	return msg.wParam;
}


/* - - - - - sandbox - - - - -

- - - - - sandbox - - - - - */
 