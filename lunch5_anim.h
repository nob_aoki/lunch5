#ifndef __LUNCH5_ANIM_H__
#define __LUNCH5_ANIM_H__

#include <windows.h>

#include <vector>

#include <cmnstring.h>

#include <fio.h>


class CAnimation
{
public:
    enum {
        DRAW_FIX = 0,   // ボタンを押した側固定
        DRAW_LEFTFIX,   // 固定
        DRAW_SHORTEN,   // 拡大縮小
        DRAW_SLIDE,     // ボタンを押していない側固定
        DRAW_RIGHTFIX,  // 右側固定

        DRAW_SIZE,

        DRAW_DEFAULT = DRAW_FIX
    };

public:
    CAnimation();
    virtual ~CAnimation();
    int Open(CStdio *file);

    void WriteXML(CStdio *file) {
        file->Puts(" <animation>\n");
        file->Print("  <move>%s</move>\n", id_);
        file->Puts("  <draw>");
        switch (type_draw_) {
        case DRAW_FIX:
        case DRAW_LEFTFIX:
        case DRAW_SHORTEN:
        case DRAW_SLIDE:
        case DRAW_RIGHTFIX:
            file->Puts(strdrawtbl_[type_draw_]);
            break;
        default:
            file->Puts(strdrawtbl_[DRAW_DEFAULT]);
            break;
        }
        file->Puts("</draw>\n");
        file->Puts(" </animation>\n");
    }
    void SetDrawType(int type) {
        type_draw_ = type;
    }
    void SetMoveType(const char*str) {
        id_ = str;
    }
private:
    int type_draw_;
    CString path_move_;
    CString id_;
    std::vector<long int> pos_;

public:
    static int DrawTypeFromString(const char* strtype) {
        for (int i = 0 ; i < DRAW_SIZE ; ++i) {
            if (stricmp(strtype, strdrawtbl_[i]) == 0) {
                return i;
            }
        }
        return DRAW_DEFAULT;
    }

    static const char* strdrawtbl_[DRAW_SIZE];
};


#endif  // __LUNCH5_ANIM_H__
