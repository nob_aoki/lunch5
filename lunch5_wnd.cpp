/** 
 * @file lunch5_wnd.cpp
 * @brief ここに説明を書く
 */

#include <windows.h>

#include "lunch5_wnd.h"

#include <cstdio>
#include <cmath>

#include <commctrl.h>

#include <wndutil.h>
#include <imagelib.h>
#include <fio.h>

#include "resource.h"

#include "lunch5_main.h"


#if _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#define new ::new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif

//------------------------------------------------
// 定数
//------------------------------------------------

const char* CMainWindow::szClassName = "lunch5_mainwindow";
const char* CMainWindow::szTitle = "Lunch5 main";

//------------------------------------------------
// 変数
//------------------------------------------------

/* -- -- -- -- -- -- command handler -- -- -- -- -- -- */
/*int CMainWindow::OnCommand(WPARAM, LPARAM);
void CMainWindow::OnOpen();
void CMainWindow::OnAbout();
void CMainWindow::OnHelp();*/

/* -- -- -- -- -- -- window handler -- -- -- -- -- -- */

void CMainWindow::OnTimer(int ID)
{

}

/**
 * ウインドウの初期化
 */
void CMainWindow::OnCreate(LPCREATESTRUCT lpcs)
{
    EnableWindow(FALSE);

    CenterWindow(GetDesktopWindow(), hWnd_);
}

void CMainWindow::OnSize(UINT/*uFlag*/, int/* width*/, int/* height*/)
{
    int width_left, tbheight;
    RECT rc1, rc2, rc3;  // ,rc4,rc5;
}


void CMainWindow::OnDropFiles(HDROP hDrop)
{
    /*char filename[MAX_PATH];

    DragQueryFile(hDrop, 0, filename, sizeof(filename));
    DragFinish(hDrop);

    OpenFile(filename);*/
}


int CMainWindow::OnNotify(LPNMHDR pnmhdr)
{
    return 0;
}

void CMainWindow::OnPaint()
{
    PAINTSTRUCT ps;
    CDC dc(BeginPaint(hWnd_, &ps));


    EndPaint(hWnd_, &ps);
}

void CMainWindow::OnClose()
{
    DestroyWindow();
}

/**
 *    MainDlgProc
 */
LRESULT CALLBACK CMainWindow::WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp)
{
    CMainWindow*pwnd;
    switch (msg) {
    // case WM_SIZE:
        // pwnd = CMainWindow::FromHWND(hWnd);
        // pwnd->OnSize(wp, LOWORD(lp), HIWORD(lp));
        // break;
    // case WM_PAINT:
        // pwnd = CMainWindow::FromHWND(hWnd);
        // pwnd->OnPaint();
        // break;
    // case WM_COMMAND:
        // pwnd = CMainWindow::FromHWND(hWnd);
        // return pwnd->OnCommand(wp, lp);
        // break;
    case WM_NCCREATE:
        pwnd = new CMainWindow(hWnd);
        pwnd->Register();
        //return TRUE;
        return DefWindowProc(hWnd,msg,wp,lp);
        break;
    case WM_CREATE:
        pwnd = CMainWindow::FromHWND(hWnd);
        pwnd->OnCreate((LPCREATESTRUCT)lp);
        break;
    // case WM_DROPFILES:
        // pwnd = CMainWindow::FromHWND(hWnd);
        // pwnd->OnDropFiles((HDROP)wp);
        // break;
    // case WM_TIMER:
        // pwnd = CMainWindow::FromHWND(hWnd);
        // pwnd->OnTimer(wp);
        // break;
    // case WM_NOTIFY:
        // pwnd = CMainWindow::FromHWND(hWnd);
        // return pwnd->OnNotify((LPNMHDR)lp);
        // break;
    case WM_CLOSE:
        pwnd = CMainWindow::FromHWND(hWnd);
        pwnd->OnClose();
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_NCDESTROY:
        pwnd = CMainWindow::FromHWND(hWnd);
        delete pwnd;
        break;
    default:
        return DefWindowProc(hWnd,msg,wp,lp);
    }
    return 0;
}

/* -- -- -- -- -- utility functions -- -- -- -- -- */

HWND CMainWindow::Create(HINSTANCE hInstance)
{
    WNDCLASSEX wc;
    memset(&wc, 0, sizeof(WNDCLASSEX));
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.hInstance = hInstance;
    wc.style = CS_HREDRAW | CS_VREDRAW /*|CS_DBLCLKS*/;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.lpszMenuName = NULL;
    wc.lpfnWndProc = CMainWindow::WndProc;
    // wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wc.hbrBackground = (HBRUSH)(COLOR_BTNFACE+1);
    // wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
    wc.lpszClassName = (LPCTSTR)CMainWindow::szClassName;
    wc.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wc.hIconSm = (HICON)LoadImage(hInstance, MAKEINTRESOURCE(IDI_ICON1),
            IMAGE_ICON, 16, 16, LR_DEFAULTCOLOR);
    RegisterClassEx(&wc);

    HWND hWnd = CreateWindowEx(0/*WS_EX_ACCEPTFILES*/,
        CMainWindow::szClassName,
        CMainWindow::szTitle, WS_POPUP | WS_CLIPCHILDREN,
        // CW_USEDEFAULT, 0, CW_USEDEFAULT, 0,
        CW_USEDEFAULT, 0, 100, 100,
        NULL, NULL, hInstance, NULL);

    return hWnd;
}

/** 
 * ここに説明を書く
 * 
 * @param width ウインドウの幅
 * @param height ウインドウの高さ
 * @param fixcorner 基準にするウインドウの角
 *                  FIXCORNER_LEFTTOP, FIXCORNER_RIGHTTOP
 *                  FIXCORNER_LEFTBOTTOM, FIXCORNER_RIGHTBOTTOM
 * @author (株)トーメーコーポレーション  青木 伸頼
 * @date 2012.11.14
 * @note 2012.11.14 (株)トーメーコーポレーション  青木 伸頼 作成
 * @note status:テスト待ち、完成1、リファクタ、完成2
 */
void CMainWindow::Resize(int width, int height, int fixcorner)
{
    RECT rc;
    GetWindowRect(&rc);

    int x, y;
    if (fixcorner == 0) {
        // left-top
        x = rc.left;
        y = rc.top;
    } else if (fixcorner == 1) {
        // right-top
        x = rc.right-width;
        y = rc.top;
    } else if (fixcorner == 2) {
        // left-bottom
        x = rc.left;
        y = rc.bottom-height;
    } else if (fixcorner == 3) {
        // right-bottom
        x = rc.right-width;
        y = rc.bottom-height;
    } else {
        x = rc.left;
        y = rc.top;
    }

    SetWindowPos(NULL, x, y, width, height, SWP_NOZORDER);
}

/* - - - - - sandbox - - - - -

- - - - - sandbox - - - - - */
 
