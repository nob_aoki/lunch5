/** 
 * @file lunch5_button.h
 * @brief ここに説明を書く
 * 
 * @note 2012.05.14 (株)トーメーコーポレーション  青木 伸頼 作成
 * @version $Id: lunch5_button.h,v 1.00 2012/05/14 18:00:37 aoki Exp $
 */
#ifndef __LUNCH5_BUTTON_H__
#define __LUNCH5_BUTTON_H__

// #include "lunch5_wndbase.h"

enum {
    ICON_WIDTH = 32,
    ICON_HEIGHT = ICON_WIDTH,
    MARGIN_X = 3,
    MARGIN_Y = 3,
    BUTTON_WIDTH  = ICON_WIDTH+MARGIN_X*2,
    BUTTON_HEIGHT = ICON_HEIGHT+MARGIN_Y*2,
    BUTTON_LR_WIDTH = 20,
    BUTTON_LR_HEIGHT = BUTTON_HEIGHT,
};

class CButtonBase : public CWindowBase
{
public:
    enum {
        PUSHED = 0,
        NOT_PUSHED,

        INVALID = -1,
        LEFT = 0,
        RIGHT,
    };
public:
    CButtonBase()
        :pItem_(NULL), LR_(INVALID){}
    ~CButtonBase() {}

    virtual int Clicked() = 0;
    int SetImage(CImageLib &back, HICON hicon = NULL);

#if 0
    int Clicked() {
        if (pItem_) {
            pItem_->Exec();
        }
    }
#endif
    void SetItem(CItem *pitem) {
        pItem_ = pitem;
    }
    void Draw(CDC *pdc, int ox, int oy, int status) {
        int srcy;
        int height = image.Height()/2;
        if (status == PUSHED) {
            srcy = height;
        } else {
            srcy = 0;
        }
        image_.Draw(pdc, ox, oy, image.Width(), height, 0, srcy);
    }

    void OnTimer(int ID);
    void OnCreate(LPCREATESTRUCT lpcs);
    void OnSize(UINT uFlag, int width, int height);
    void OnGetMinMaxInfo(LPMINMAXINFO pmmi);
    void OnDropFiles(HDROP hDrop);
    int OnNotify(LPNMHDR pnmhdr);
    void OnClose();
    void OnPaint();
    void OnLButtonDown();
    void OnLButtonUp();
    void OnMouseMove();
    int OnCommand(WPARAM, LPARAM);

private:
    CImageLib image_;
    CItem *item_;
    int state_;  // none, pushed, clicked, dblclicked,

public:
    static CButtonBase *FromHWND(HWND hWnd) {
        return reinterpret_cast<CButtonBase*>(
            ::GetWindowLongPtr(hWnd, GWLP_USERDATA);
    }
    static HWND Create(HINSTANCE hInstance);
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wp, LPARAM lp);

private:
    static const char* szClassName;
    static const char* szTitle;
};

class CMyButton: public CButtonBase
{
public:
    CMyButton() {}
    virtual int Clicked();  // コマンドの実行
};

class CRLButton: public CButtonBase
{
public:
    enum {
        VISIBLE,
        DISABLE,
        HIDDEN,

        DEFAULT_VISIBLE = VISIBLE
    };
public:
    CRLButton():visible_(DEFULT_VISIBLE) {}

    virtual int Clicked();  // ウィンドウの変形
    int isVisible() {
        return (visible_ == VISIBLE || visible_ == DISABLE);
    }
    int GetVisible() {return visible_;}

    // @note ウィンドウの表示非表示も行う。
    void SetVisible(int state) {
        switch (state) {
        case VISIBLE:
            ShowWindow(SW_SHOW);
            break;
        case DISABLE:
            break;
        case HIDDEN:
            ShowWindow(SW_HIDE);
            break;
        default:
            SetVisible(DEFAULT_VISIBLE);
            return;
            break;
        }
        visible_ = state;
    }
private:
    int LR_;
    int visible_;  // 
};

class CButtonManager
{
public:
    CButtonManager() {}
    ~CButtonManager() {
        if (buttons_.size() != 0) {
            for (std::vector<CMyButton*>::iterator itr = buttons_.begin() ;
                 itr != buttons_.end() ; ++itr) {
                delete *itr;
            }
        }
    }
    // 開いた時の幅
    int FullSize() {
        int sz = 0;
        if (left_->IsVisible() != 0)
            sz = BUTTON_LR_WIDTH;
        sz += buttons_.size() * BUTTON_WIDTH;
        if (right_->IsVisible() != 0)
            sz += BUTTON_LR_WIDTH;
    }
    // 閉じた時の幅
    int MinimumSize() {
        int sz = 0;
        if (left_->IsVisible() != 0)
            sz = BUTTON_LR_WIDTH;
        if (right_->IsVisible() != 0)
            sz += BUTTON_LR_WIDTH;
    }
    void Add(CMyButton *pbtn) {
        buttons_.push_back(pbtn);
    }
    void SetRLButtonState(int nbtn, int state) {
        if (nbtn == CButtonBase::LEFT) {
            left_.SetVisible(state);
        } else if (nbtn == CButtonBase::RIGHT) {
            right_.SetVisible(state);
        }
    }

    // 開閉時の各ボタンの移動と変形
    int ArrangePosition(int mode, int width);

private:
    CRLButton left_;
    CRLButton right_;
    std::vector<CMyButton*> buttons_;
};

#endif  // __LUNCH5_BUTTON_H__
